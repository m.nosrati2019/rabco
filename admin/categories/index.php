<?php
include "../../bootstrap/boot.php";
use App\Database\DB;
use App\Http\Controllers\Admin\CategoryController;

new CategoryController();
$db = new DB();
$categories = $db->select("SELECT * FROM categories ORDER BY id DESC",[],0);

view("admin.categories.index",compact("categories"));

