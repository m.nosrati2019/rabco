<?php
include "../../bootstrap/boot.php";
use App\Database\DB;
use App\Http\Controllers\Admin\CategoryController as Category;
$category = new Category();
$db = new DB();
$categories = $db->select("SELECT * FROM categories",[],0);
$store = '';
$submit = request()->input("create-categories");
if ($_SERVER['REQUEST_METHOD'] == 'POST' and isset($submit)){
    $store = is_array($category->store()) ?  $category->store() : "دسته بندی با موفقیت اضافه شد";
}
view("admin.categories.create",compact('categories','store'));


