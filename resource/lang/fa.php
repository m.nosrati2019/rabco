<?php
return $lang = [
    'title' => 'عنوان',
    'parent_id' => 'والد',
    'description' => 'توضیحات',
    'email' => 'ایمیل',
    'password' => 'کلمه عبور',
    'confirm' => 'کلمه عبور و تکرار آن باید یکسان باشد!',
    'name'=>'نام و نام خانوادگی',
    'confirm-password' => 'تکرار کلمه عبور'
];