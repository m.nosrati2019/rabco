<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <title>فروشگاه ربکو</title>
    <link rel="stylesheet" href="<?php assets("front/css/bootstrap.min.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/animate.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/icofont.min.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/swiper.min.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/jquery.fancybox.min.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/nouislider.min.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/icheck/all.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/style.css"); ?>">
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3">
                <div class="logo">
                    <img src="<?php assets("front/images/logo.png"); ?>" alt="logo">
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                <div class="content">
                    <form>
                        <input type="text" name="search" placeholder="جستجو در ربکو...">
                        <button><i class="icofont-search-2"></i></button>
                    </form>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3">
                <div class="content">
                    <div class="cart">
                        <div class="cart-modal">
                    <span class="clear-cart">
                    <i class="icofont-bag"></i>
                        سبد خرید شما خالی است!
                        </span>
                        </div>
                        <i class="icofont-shopping-cart"></i>
                        <span class="counter">0</span>
                    </div>
                    <span class="account">
                    <i class="icofont-ui-user"></i>
                </span>
                </div>


            </div>
        </div>
    </div>
    <div class="nav-bar-rab">
        <div class="container">
            <div class="row">
                <nav class="menu-top">
                    <ul>
                        <li><a href="">کالای دیجیتال</a>
                            <div class="menu-2">
                        <span class="col-md-3 span-menu-2">
                            <ul>
                                <li class="tow-menu"><a href="">لپ تاپ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                            </ul>
                        </span>
                                <span class="col-md-3 span-menu-2">
                            <ul>
                                <li class="tow-menu"><a href="">لپ تاپ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                            </ul>
                        </span>
                                <span class="col-md-3 span-menu-2">
                            <ul>
                                <li class="tow-menu"><a href="">لپ تاپ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                            </ul>
                        </span>
                                <span class="col-md-3 span-menu-2">
                            <ul>
                                <li class="tow-menu"><a href="">لپ تاپ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                                <li><a href="">سامسونگ</a></li>
                            </ul>
                        </span>
                            </div>
                        </li>
                        <li><a href="">لوازم خانگی </a>
                            <div class="menu-2">
                        <span class="col-md-3 span-menu-2">
                            <ul>
                                <li class="tow-menu"><a href="">یخچال</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                            </ul>
                        </span>
                                <span class="col-md-3 span-menu-2">
                            <ul>
                                <li class="tow-menu"><a href="">یخچال</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                            </ul>
                        </span>
                                <span class="col-md-3 span-menu-2">
                            <ul>
                                <li class="tow-menu"><a href="">یخچال</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                            </ul>
                        </span>
                                <span class="col-md-3 span-menu-2">
                            <ul>
                                <li class="tow-menu"><a href="">یخچال</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                                <li><a href="">الجی</a></li>
                            </ul>
                        </span>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>