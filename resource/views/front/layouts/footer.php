<footer>
    <div class="inner-box-footer">
        <div class="container">
            <div class="row">
                <div class="inner-rab">
                    <div class="section">
                        <i class="icofont-send-mail"></i>
                        <span>
                            <h3>تحویل اکسپرس</h3>
                            <h4>در کمترین زمان دریافت کنید</h4>
                        </span>

                    </div>

                    <div class="section">
                        <i class="icofont-support"></i>
                        <span>
                            <h3>پشتیبانی ۲۴ ساعته</h3>
                            <h4>پشتیبانی هفت روز هفته</h4>
                        </span>

                    </div>

                    <div class="section">
                        <i class="icofont-pay"></i>
                        <span>
                            <h3>پرداخت آنلاین</h3>
                            <h4>بی دردسر پرداخت کنید</h4>
                        </span>

                    </div>

                    <div class="section">
                        <i class="icofont-history"></i>
                        <span>
                            <h3>۷ روز ضمانت بازگشت</h3>
                            <h4>هفت روز مهلت دارید</h4>
                        </span>

                    </div>

                    <div class="section">
                        <i class="icofont-badge"></i>
                        <span>
                            <h3>ضمانت اصل‌بودن کالا</h3>
                            <h4>تایید اصالت کالا</h4>
                        </span>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2">
                    <div class="menu-footer">
                        <h6>خدمات ربکو</h6>
                        <ul>
                            <li><a href="">آموزش برنامه نویسی</a></li>
                            <li><a href="">طراحی سایت</a></li>
                            <li><a href="">طراحی سایت</a></li>
                            <li><a href="">برنامه نویسی</a></li>
                            <li><a href="">برنامه نویسی</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2">
                    <div class="menu-footer">
                        <h6>خدمات ربکو</h6>
                        <ul>
                            <li><a href="">آموزش برنامه نویسی</a></li>
                            <li><a href="">طراحی سایت</a></li>
                            <li><a href="">طراحی سایت</a></li>
                            <li><a href="">برنامه نویسی</a></li>
                            <li><a href="">برنامه نویسی</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2">
                    <div class="menu-footer">
                        <h6>خدمات ربکو</h6>
                        <ul>
                            <li><a href="">آموزش برنامه نویسی</a></li>
                            <li><a href="">طراحی سایت</a></li>
                            <li><a href="">طراحی سایت</a></li>
                            <li><a href="">برنامه نویسی</a></li>
                            <li><a href="">برنامه نویسی</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                    <div class="about-us">
                        <h5>اندکی در باره ربکو</h5>
                        <p>گروه برنامه نویسی و فروشگاهی ربکو در سال 1390 فعالیت خود را بر بستر وب آغاز نموده است و تا به امروز خیلی خوب بوده است</p>
                    </div>
                    <div class="model">
                        <span><img src="<?php assets("front/images/logo.aspx_.png"); ?>" alt="nemad"></span>
                        <span><img src="<?php assets("front/images/50a7c6fb.png"); ?>" alt="nemad"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-footer">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <p class="float-right">تمامی حقوق متعلق بع گروه رایانه افزار بییستون است و هرگونه کپی برداری پیگرد قانونی دارد.</p>

                </div>
                <div class="col-md-3">
                    <a class="float-left">
                        طراحی و اجرا گروه ربکو
                    </a>
                </div>
            </div>
        </div>
    </div>


</footer>
<script src="<?php assets("front/js/jquery-3.4.1.min.js"); ?>"></script>
<script src="<?php assets("front/js/bootstrap.min.js"); ?>"></script>
<script src="<?php assets("front/js/jquery.mobile.custom.min.js"); ?>"></script>
<script src="<?php assets("front/js/wow.js"); ?>"></script>
<script src="<?php assets("front/js/swiper/swiper.min.js"); ?>"></script>
<script src="<?php assets("front/js/jquery.syotimer.min.js"); ?>"></script>
<script src="<?php assets("front/js/jquery.elevateZoom-3.0.8.min.js"); ?>"></script>
<script src="<?php assets("front/js/jquery.fancybox.min.js"); ?>"></script>
<script src="<?php assets("front/js/nouislider.min.js"); ?>"></script>
<script src="<?php assets("front/js/customclass.js"); ?>"></script>
<script src="<?php assets("front/js/icheck.min.js"); ?>"></script>
<script src="<?php assets("front/js/main.js"); ?>"></script>
</body>
</html>