<?php require_once VIEW_PATH."front".DS."layouts".DS."header.php"?>

    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3 float-md-right">
                    <div class="banner-left-slider">
                        <div class="img-banner-left">
                            <img src="<?php assets("front/images/banner-left.jpg"); ?>" alt="img">
                        </div>
                        <div class="img-banner-left">
                            <img src="<?php assets("front/images/banner-left2.jpg"); ?>" alt="img">
                        </div>
                        <div class="img-banner-left">
                            <img src="<?php assets("front/images/banner-left.jpg"); ?>" alt="img">
                        </div>
                        <div class="img-banner-left">
                            <img src="<?php assets("front/images/banner-left2.jpg"); ?>" alt="img">
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9 col-lg-9">
                    <div class="slider slider-main">
                        <div class="swiper-container main-slider">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide main-item">
                                    <img src="<?php assets("front/images/1.jpg"); ?>" alt="slider1">
                                </div>
                                <div class="swiper-slide main-item">
                                    <img src="<?php assets("front/images/2.jpg"); ?>" alt="slider1">
                                </div>
                                <div class="swiper-slide main-item">
                                    <img src="<?php assets("front/images/3.jpg"); ?>" alt="slider1">
                                </div>
                                <div class="swiper-slide main-item">
                                    <img src="<?php assets("front/images/4.jpg"); ?>" alt="slider1">
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>
                    <div class="slider slider-vip-model">
                        <div class="swiper-container slider-vip">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide swiper-slide2">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="img-slider">
                                                <img src="<?php assets("front/images/61FaWQvnFxL._SL1000_-400x400.jpg"); ?>" alt="product">
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                            <div class="slider-info">
                                                <h1>محصول شمااره یک</h1>
                                                <div class="price-div">
                                                    <span class="price">250،000 تومان</span>
                                                    <span class="price-old">300،000 تومان</span>
                                                </div>
                                                <div class="attr">
                                                    <span>ویژگی ول</span>
                                                    <span>ویژگی دوم</span>
                                                    <span>ویژگی سوم</span>
                                                </div>
                                            </div>
                                            <div class="timer" data-year="2020" data-month="5" data-day="10" data-hour="10" data-minute="12"></div>
                                        </div>

                                    </div>
                                </div>
                                <div class="swiper-slide swiper-slide2">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="img-slider">
                                                <img src="<?php assets("front/images/61FaWQvnFxL._SL1000_-400x400.jpg"); ?>" alt="product">
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                            <div class="slider-info">
                                                <h1>محصول شمااره یک</h1>
                                                <div class="price-div">
                                                    <span class="price">250،000 تومان</span>
                                                    <span class="price-old">300،000 تومان</span>
                                                </div>
                                                <div class="attr">
                                                    <span>ویژگی ول</span>
                                                    <span>ویژگی دوم</span>
                                                    <span>ویژگی سوم</span>
                                                </div>
                                            </div>
                                            <div class="timer" data-year="2020" data-month="12" data-day="15" data-hour="3" data-minute="6"></div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--    banner-->
        <div class="banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
                        <div class="banner-img">
                            <img src="<?php assets("front/images/1000021913.jpg"); ?>" alt="banner">
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
                        <div class="banner-img">
                            <img src="<?php assets("front/images/1000022075.jpg"); ?>" alt="banner">
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
                        <div class="banner-img">
                            <img src="<?php assets("front/images/1000022151.jpg"); ?>" alt="banner">
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
                        <div class="banner-img">
                            <img src="<?php assets("front/images/1000022159.jpg"); ?>" alt="banner">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--    banner-->

        <div class="slider-last-product">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2">
                        <div class="img">
                            <img src="<?php assets("front/images/8af90c4b.png"); ?>" alt="images">
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-10 col-lg-10">
                        <div class="slider-product slider-product-model">
                            <div class="swiper-container last-product-rab">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p1.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p2.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p3.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p4.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p5.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>


                                </div>
                                <!-- Add Pagination -->
                                <div class="swiper-button-next next-arrow-rab"></div>
                                <div class="swiper-button-prev prev-arrow-rab"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="slider-last-product slider-last-tow">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2">
                        <div class="img">
                            <img src="<?php assets("front/images/8af90c4b.png"); ?>" alt="images">
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-10 col-lg-10">
                        <div class="slider-product slider-product-model">
                            <div class="swiper-container last-product-rab">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p1.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p2.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p3.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p4.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p5.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>


                                </div>
                                <!-- Add Pagination -->
                                <div class="swiper-button-next next-arrow-rab"></div>
                                <div class="swiper-button-prev prev-arrow-rab"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="slider-last-product slider-last-three">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2">
                        <div class="img">
                            <img src="<?php assets("front/images/8af90c4b.png"); ?>" alt="images">
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-10 col-lg-10">
                        <div class="slider-product slider-product-model">
                            <div class="swiper-container last-product-rab">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p1.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p2.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p3.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p4.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p5.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="price">1،200،000 تومان</span>
                                            <span class="discount">40% تخفیف</span>
                                        </div>
                                    </div>


                                </div>
                                <!-- Add Pagination -->
                                <div class="swiper-button-next next-arrow-rab"></div>
                                <div class="swiper-button-prev prev-arrow-rab"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="category-count">
            <h2>بیش از ۲،۰۰۰،۰۰۰ کالا در دسته‌بندی‌های مختلف</h2>
            <div class="container">
                <div class="row">
                    <div class="categories">
                        <div class="category">
                            <i class="icofont-monitor"></i>
                            <h5>مانیتور</h5>
                            <h6>+250000 کالا</h6>
                        </div>
                        <div class="category">
                            <i class="icofont-monitor"></i>
                            <h5>نام دسته بندی</h5>
                            <h6>+250000 کالا</h6>
                        </div>
                        <div class="category">
                            <i class="icofont-monitor"></i>
                            <h5>نام دسته بندی</h5>
                            <h6>+250000 کالا</h6>
                        </div>
                        <div class="category">
                            <i class="icofont-monitor"></i>
                            <h5>نام دسته بندی</h5>
                            <h6>+250000 کالا</h6>
                        </div>
                        <div class="category">
                            <i class="icofont-monitor"></i>
                            <h5>نام دسته بندی</h5>
                            <h6>+250000 کالا</h6>
                        </div>
                        <div class="category">
                            <i class="icofont-monitor"></i>
                            <h5>نام دسته بندی</h5>
                            <h6>+250000 کالا</h6>
                        </div>
                        <div class="category">
                            <i class="icofont-monitor"></i>
                            <h5>نام دسته بندی</h5>
                            <h6>+250000 کالا</h6>
                        </div>
                        <div class="category">
                            <i class="icofont-monitor"></i>
                            <h5>نام دسته بندی</h5>
                            <h6>+250000 کالا</h6>
                        </div>
                        <div class="category">
                            <i class="icofont-monitor"></i>
                            <h5>نام دسته بندی</h5>
                            <h6>+250000 کالا</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="slider-last-product slider-blog">
            <h1>آخرین مطالب وبلاگ</h1>
            <div class="container">
                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                        <div class="slider-product slider-product-model">
                            <div class="swiper-container last-product-rab blog-slider-rab">
                                <div class="swiper-wrapper">

                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p2.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="discount float-left">30 نظر</span>
                                            <span class="price float-right">نویسنده : میثم نصرتی</span>

                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p2.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="discount float-left">30 نظر</span>
                                            <span class="price float-right">نویسنده : میثم نصرتی</span>

                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p2.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="discount float-left">30 نظر</span>
                                            <span class="price float-right">نویسنده : میثم نصرتی</span>

                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p2.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="discount float-left">30 نظر</span>
                                            <span class="price float-right">نویسنده : میثم نصرتی</span>

                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p2.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="discount float-left">30 نظر</span>
                                            <span class="price float-right">نویسنده : میثم نصرتی</span>

                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p2.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="discount float-left">30 نظر</span>
                                            <span class="price float-right">نویسنده : میثم نصرتی</span>

                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-last">
                                        <div class="info">
                                            <img src="<?php assets("front/images/p2.jpg"); ?>" alt="products">
                                            <h1>محصول شماره یک گوشی موبایل سامسونگ مدل اول و دوم</h1>
                                            <span class="discount float-left">30 نظر</span>
                                            <span class="price float-right">نویسنده : میثم نصرتی</span>

                                        </div>
                                    </div>


                                </div>
                                <!-- Add Pagination -->
                                <div class="swiper-button-next next-arrow-rab"></div>
                                <div class="swiper-button-prev prev-arrow-rab"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php require_once VIEW_PATH."front".DS."layouts".DS."footer.php"?>