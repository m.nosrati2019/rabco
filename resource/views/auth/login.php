<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <title>فروشگاه ربکو</title>
    <link rel="stylesheet" href="<?php assets("front/css/bootstrap.min.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/animate.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/icofont.min.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/swiper.min.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/jquery.fancybox.min.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/nouislider.min.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/icheck/all.css"); ?>">
    <link rel="stylesheet" href="<?php assets("front/css/style.css"); ?>">
</head>
<body>
<div class="main">
    <div class="box-register">
        <div class="container">
            <div class="row">
                <div class="logo">
                    <img src="<?php assets("front/images/logo.png") ?>" alt="logo">
                </div>
                <div class="col-md-4 margin-0-auto">
                    <div class="register">

                        <?php if ($errors != ''){
                            if (is_array($errors)){
                                ?>
                                <?php foreach ($errors as $error){ ?>
                                    <div class="alert alert-danger text-right" dir="rtl"><?= $error ?></div>
                                    <?php
                                }
                            }else{ ?>
                                <div class="alert alert-success text-right" dir="rtl"><?= $store ?></div>
                                <?php
                            }
                        }
                        ?>
                        <h1>ورود به ربکو</h1>
                        <div class="form-register">
                            <form action="<?php action("login"); ?>" method="post">
                                <div class="form-group form-icon-email">
                                    <label>ایمیل : </label>
                                    <i class="icofont-user"></i>
                                    <input type="email" name="email" value="<?= old("email") ?>" class="form-control">
                                </div>
                                <div class="form-group form-icon-password">
                                    <label>کلمه عبور : </label>
                                    <i class="icofont-lock"></i>
                                    <input type="password" name="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button name="login-button" type="submit" class="btn-black-rab">
                                        <i class="icofont-login"></i>
                                        ورود
                                    </button>

                                </div>
                                <span class="rules">
                        کلمه عبور خود را
                        <a href="">
                        فراموش
                        </a>
                        کرده ام!
                    </span>
                                <span class="rules">
                        <a href="">
                        حساب کاربری ندارم
                        </a>
                    </span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<script src="<?php assets("front/js/jquery-3.4.1.min.js"); ?>"></script>
<script src="<?php assets("front/js/bootstrap.min.js"); ?>"></script>
<script src="<?php assets("front/js/jquery.mobile.custom.min.js"); ?>"></script>
<script src="<?php assets("front/js/wow.js"); ?>"></script>
<script src="<?php assets("front/js/swiper/swiper.min.js"); ?>"></script>
<script src="<?php assets("front/js/jquery.syotimer.min.js"); ?>"></script>
<script src="<?php assets("front/js/jquery.elevateZoom-3.0.8.min.js"); ?>"></script>
<script src="<?php assets("front/js/jquery.fancybox.min.js"); ?>"></script>
<script src="<?php assets("front/js/nouislider.min.js"); ?>"></script>
<script src="<?php assets("front/js/customclass.js"); ?>"></script>
<script src="<?php assets("front/js/icheck.min.js"); ?>"></script>
<script src="<?php assets("front/js/main.js"); ?>"></script>

</body>
</html>