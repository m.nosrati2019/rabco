<?php require_once VIEW_PATH."admin".DS."inc".DS."header.php"?>
<?php require_once VIEW_PATH."admin".DS."inc".DS."sidebar.php"?>
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>دسته بندی ها</h2>
        </div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">

                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<?php action("admin.categories.create"); ?>">اضافه کردن دسته بندی</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>عنوان</th>
                            <th>توضیحات</th>
                            <th>تاریخ ایجاد</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php foreach ($categories as $category){ ?>
                        <tr>
                            <td><?= $category->title ?></td>
                            <td><?= $category->description ?></td>
                            <td><?= \Hekmatinasser\Verta\Verta::instance($category->created_at); ?></td>
                        </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
<?php require_once VIEW_PATH."admin".DS."inc".DS."footer.php"?>
