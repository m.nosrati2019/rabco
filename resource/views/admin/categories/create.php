<?php require_once VIEW_PATH."admin".DS."inc".DS."header.php"?>
<?php require_once VIEW_PATH."admin".DS."inc".DS."sidebar.php"?>
<section class="content">
    <?php if ($store != ''){
        if (is_array($store)){
            ?>
            <?php foreach ($store as $error){ ?>
                <div class="alert alert-danger text-right" dir="rtl"><?= $error ?></div>
            <?php
            }
        }else{ ?>
                <div class="alert alert-success text-right" dir="rtl"><?= $store ?></div>
            <?php
        }
    }
    ?>


    <div class="container-fluid">
        <div class="block-header">
            <h2>اضافه کردن دسته جدید</h2>
        </div>

        <div class="row clearfix">
            <form action="<?php action("admin.categories.create"); ?>" method="post">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                    <div class="form-line">
                        <input type="text" name="title" class="form-control date" placeholder="عنوان دسته بندی">
                    </div>
                </div>

                <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                    <div class="form-line">
                        <input type="text" name="description" class="form-control date" placeholder="توضیحات">
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="input-group">
                <label for="parent_id">دسته والد</label><br>
                <select name="parent_id" class="form-control" multiple>
                    <option selected value="0">انتخاب کنید</option>
                    <?php foreach ($categories as $category) {?>
                        <option value="<?= $category->id ?>"><?= $category->title ?></option>
                    <?php }?>
                </select>
                </div>
                <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                    <div class="form-line">
                        <input type="submit" name="create-categories" class="form-control date btn btn-success" value="ایجاد دسته بندی">
                    </div>
                </div>
            </div>
            </div>
            </form>
        </div>
    </div>
</section>
<?php require_once VIEW_PATH."admin".DS."inc".DS."footer.php"?>
