
<!-- Jquery Core Js -->
<script src="<?php assets("backend/plugins/jquery/jquery.min.js") ?>"></script>

<!-- Bootstrap Core Js -->
<script src="<?php assets("backend/plugins/bootstrap/js/bootstrap.js") ?>"></script>

<!-- Select Plugin Js -->

<!-- Slimscroll Plugin Js -->
<script src="<?php assets("backend/plugins/jquery-slimscroll/jquery.slimscroll.js") ?>"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?php assets("backend/plugins/node-waves/waves.js") ?>"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="<?php assets("backend/plugins/jquery-countto/jquery.countTo.js") ?>"></script>

<!-- Morris Plugin Js -->
<script src="<?php assets("backend/plugins/raphael/raphael.min.js") ?>"></script>
<script src="<?php assets("backend/plugins/morrisjs/morris.js") ?>"></script>

<!-- ChartJs -->
<script src="<?php assets("backend/plugins/chartjs/Chart.bundle.js") ?>"></script>

<!-- Flot Charts Plugin Js -->
<script src="<?php assets("backend/plugins/flot-charts/jquery.flot.js") ?>"></script>
<script src="<?php assets("backend/plugins/flot-charts/jquery.flot.resize.js") ?>"></script>
<script src="<?php assets("backend/plugins/flot-charts/jquery.flot.pie.js") ?>"></script>
<script src="<?php assets("backend/plugins/flot-charts/jquery.flot.categories.js") ?>"></script>
<script src="<?php assets("backend/plugins/flot-charts/jquery.flot.time.js") ?>"></script>

<!-- Sparkline Chart Plugin Js -->
<script src="<?php assets("backend/plugins/jquery-sparkline/jquery.sparkline.js") ?>"></script>
<script src="../../plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

<!-- Dropzone Plugin Js -->
<script src="<?php assets("plugins/dropzone/dropzone.js"); ?>"></script>

<!-- Input Mask Plugin Js -->
<script src="<?php assets("plugins/jquery-inputmask/jquery.inputmask.bundle.js"); ?>"></script>

<!-- Multi Select Plugin Js -->

<!-- Jquery Spinner Plugin Js -->
<script src="<?php assets("plugins/jquery-spinner/js/jquery.spinner.js"); ?>"></script>

<!-- Bootstrap Tags Input Plugin Js -->
<script src="<?php assets("plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"); ?>"></script>

<!-- noUISlider Plugin Js -->
<script src="<?php assets("plugins/nouislider/nouislider.js"); ?>"></script>
<!-- Custom Js -->
<script src="<?php assets("backend/js/admin.js") ?>"></script>
<script src="<?php assets("backend/js/pages/index.js") ?>"></script>
<script src="<?php assets("backend/js/pages/tables/jquery-datatable.js"); ?>"></script>
<script src="<?php assets("js/pages/forms/advanced-form-elements.js"); ?>"></script>
<!-- Jquery DataTable Plugin Js -->
<script src="<?php assets("backend/plugins/jquery-datatable/jquery.dataTables.js"); ?>"></script>
<script src="<?php assets("backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"); ?>"></script>
<script src="<?php assets("backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"); ?>"></script>
<script src="<?php assets("backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"); ?>"></script>
<script src="<?php assets("backend/plugins/jquery-datatable/extensions/export/jszip.min.js"); ?>"></script>
<script src="<?php assets("backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js"); ?>"></script>
<script src="<?php assets("backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js"); ?>"></script>
<script src="<?php assets("backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"); ?>"></script>
<script src="<?php assets("backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js"); ?>"></script>
<!-- Demo Js -->
<script src="<?php assets("backend/js/demo.js") ?>"></script>
</body>

</html>
