$(document).ready(function () {
    $("#imageproduct").elevateZoom({
        zoomType: "lens",
        lensSize: 200,
        scrollZoom : true,
        easing : true
    });
});
$(document).ready(function () {
    $(".gallerybox").fancybox({
        // Options will go here
    });
});
//Change the appearance of the CheckBox For Page Search
let brandBox = document.querySelector('#brand-box');
if (brandBox){
    brandBox.addEventListener('click',checkBoxControl);
}
function checkBoxControl(e) {
    if (e.target.classList.contains('checkbox-brand-search')){
        let checkBoxSpan = e.target.nextElementSibling;
        if (e.target.checked === false){
            checkBoxSpan.classList.remove('checkbox-checked-active');
            checkBoxSpan.classList.remove('icofont-check');
        }else {
            checkBoxSpan.classList.add('checkbox-checked-active');
            checkBoxSpan.classList.add('icofont-check');
        }

    }
}
//Arrow Down and Up for Page Search
let iconUPDOWN = document.querySelector('#icon-arrow-down-up');
if (iconUPDOWN){
    downUp = new Customclass();
    downUp.downUp('#icon-arrow-down-up','#check-box-modal','0','424px');
    downUp.downUp('#icon-arrow-down-up-2','#check-box-modal-2','0','250px');
}


//CheckBox available Product For Search Page
let available = document.querySelector('.label__c_checked__available');
if (available){
    available.addEventListener('click',checkBoxAvailable);
    function checkBoxAvailable(e) {
          if (e.target.classList.contains('available-checkbox')){
              let label = e.target.nextElementSibling;
              let icon = label.children[0];
              if (e.target.checked === false){
               label.style.background = '#E5E5E5';
               icon.style.right = '2px';
              }else {
                  label.style.background = '#16C5DA';
                  icon.style.right = '33px';
              }

          }
    }
}
//Top Menu
let navLi = $('.menu-top > ul > li');


navLi.mouseenter(function () {

    let subMenu =$(' > .menu-2',this);
     subMenu.show();
});

navLi.mouseleave(function () {
        let subMenu =$(' > .menu-2',this );
        subMenu.hide();
});
//slider
 new Swiper('.main-slider', {
     pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
        clickable: true,
         mousewheel: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
     autoplay: {
         delay: 4000,
         disableOnInteraction: false,
     },
});

// vip slider
new Swiper('.slider-vip', {
    direction: 'vertical',
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    autoplay: {
        delay: 5000,
        disableOnInteraction: false,
    },
});
new Swiper('.last-product-rab', {
    slidesPerView: 4,
    spaceBetween: 15,
    freeMode: true,

    autoplay: {
        delay: 5000,
        disableOnInteraction: false,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        400:{
            slidesPerView: 1,
            spaceBetween: 15,
        },
        640: {
            slidesPerView: 2,
            spaceBetween: 15,
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 15,
        },
        1024: {
            slidesPerView: 4,
            spaceBetween: 15,
        },
    }
});



$('.timer').syotimer({
    year: 2020,
    month: 5,
    day: 10,
    hour: 15,
    minute: 20
});
if (document.getElementById('min-range')){
let minRange = document.getElementById('min-range').getAttribute('data-min');
let maxRange = document.getElementById('max-range').getAttribute('data-max');

var slider = document.getElementById('range-slider');
noUiSlider.create(slider, {
    start: [parseInt(minRange), parseInt(maxRange)],
    connect: true,
    range: {
        'min': parseInt(minRange),
        'max': parseInt(maxRange)
    },
    step : 10000,
    direction: 'rtl',

});

var inputNumber = document.getElementById('input-number');
var inputNumber2 = document.getElementById('input-number-2');
var sliderMinRange = document.getElementById('ui-first-slider-min');
var sliderMaxRange = document.getElementById('ui-first-slider-max');

slider.noUiSlider.on('update', function (values, handle) {
    var value = values[handle];

    if (handle) {
        inputNumber.value = parseInt(value);
        sliderMaxRange.innerText = separate(parseInt(value));
    } else {
        inputNumber2.value = parseInt(value);
        sliderMinRange.innerText =separate(parseInt(value));
    }
});
}
function separate(Number)
{
    Number+= '';
    Number= Number.replace(',', '');
    x = Number.split('.');
    y = x[0];
    z= x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(y))
        y= y.replace(rgx, '$1' + ',' + '$2');
    return y+ z;
}
function sliderComment(tagId,valueTagId,labelTag) {
    var sliderComment = document.getElementById(tagId);
    noUiSlider.create(sliderComment, {
        start: [3],
        step: 1,
        range: {
            min: [1],
            max: [5]
        },
        direction: 'rtl',
    });

    sliderComment.noUiSlider.on('update', function( value ){
        let type;
        switch (parseInt(value)) {
            case 1:
                type="خیلی بد";
                break;
            case 2:
                type ="بد";
                break;
            case 3:
                type = "متوسط";
                break;
            case 4:
                type ="خوب";
                break;
            case 5:
                type="عالی";
                break;
        }
        let commentVal = document.getElementById(valueTagId);
        commentVal.value = parseInt(value);
        let commentLabel = document.getElementById(labelTag);
        commentLabel.textContent = type;
    });
}
if (document.getElementById('slider-comment-1')) {
for (let i=1;i<=6;i++){
    sliderComment(`slider-comment-${i}`, `slider-comment-value-${i}`, `label-slider-comment-${i}`);
}
}


let cartIcon = $('.cart');
cartIcon.mouseenter(function () {
    let subMenu =$(' > .cart-modal',this);
    subMenu.show();
});
cartIcon.mouseleave(function () {
    let subMenu =$(' > .cart-modal',this );
    subMenu.hide();
});
$(document).ready(function(){
    $('input[type="radio"]').iCheck({
        checkboxClass: 'icheckbox_flat',
        radioClass: 'iradio_flat-blue'
    });
});



// let timer = document.querySelector('.timer');
// let yearTime = timer.getAttribute('data-year');
// let monthTime = timer.getAttribute('data-month');
// let dayTime = timer.getAttribute('data-day');
// let hourTime = timer.getAttribute('data-hour');
// let minuteTime = timer.getAttribute('data-minute');



// text day,hour,minute,second for timer slide tow
// let day = document.querySelector('.timer .syotimer-cell_type_day');
// let hour = document.querySelector('.timer .syotimer-cell_type_hour');
// let minute = document.querySelector('.timer .syotimer-cell_type_minute');
// let second = document.querySelector('.timer .syotimer-cell_type_second');
//
// function createElement(classElement,text,append){
//     let div = document.createElement('div');
//     div.classList.add(classElement);
//     div.textContent = text;
//     append.appendChild(div);
// }
// createElement('timer-day','روز',day);
// createElement('timer-hour','ساعت',hour);
// createElement('timer-minute','دقیقه',minute);
// createElement('timer-second','ثانیه',second);

//Slider product





