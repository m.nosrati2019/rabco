class Customclass {
     downUp(classIcon,checkModal,heightStart,heightEnd){
        let iconDown = document.querySelector(classIcon);
        if (iconDown){
            iconDown.addEventListener('click',function (e) {
                let newTagIDown = document.createElement('i');
                newTagIDown.classList.add('icofont-rounded-down');
                let newTagIUp = document.createElement('i');
                newTagIUp.classList.add('icofont-rounded-up');
                let checkBoxModal = document.querySelector(checkModal);
                if (e.target.classList.contains('icofont-rounded-up')) {
                    e.target.parentElement.appendChild(newTagIDown);
                    e.target.remove();
                    checkBoxModal.style.height = heightStart;
                } else if (e.target.classList.contains('icofont-rounded-down')) {
                    e.target.parentElement.appendChild(newTagIUp);
                    e.target.remove();
                    checkBoxModal.style.height = heightEnd;
                }
            });
        }
    }
}