<?php


namespace App\Request;


class Request
{
    public function input($filed,$post=true)
    {
       if ($this->is_post() and $post){
          return isset($_POST[$filed]) ? htmlspecialchars($_POST[$filed]) : null;
       }
        return isset($_GET[$filed]) ? htmlspecialchars($_POST[$filed]) : null;
    }

    public function all($post=true)
    {
        if ($this->is_post() and $post){
            return isset($_POST) ? array_map('htmlspecialchars',$_POST) : null;
        }
        return isset($_GET) ? array_map('htmlspecialchars',$_GET) : null;
    }
    public function is_post()
    {
        return $_SERVER['REQUEST_METHOD'] == true ? true : false;
    }

}