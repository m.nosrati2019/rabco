<?php namespace App\Http\Controllers\Auth;


use App\Database\DB;
use App\Request\Validation;

class Register
{
    public function __construct()
    {
        if (isLogin()){
            redirect('/');
        }
    }
    public function register()
    {
        $validation = new Validation();
        $rules = [
            "name"=>"required|min:3|max:100",
            "email"=>"required|email|unique:users",
            'password'=>"required|min:8|max:32",
            'confirm-password' => "required|confirm"
        ];

        if ($validation->make($rules,request()->all()) == true){
            if ($this->registerUser() == true){
                redirect('/login.php');
            }
            $errors = ["در هنگام ثبت نام مشکلی به وجود آمد لطفا مجددا اقدام فرمایید."];
            return $errors;
        }
        $errors = $validation->getError();
        return $errors;
    }

    private function registerUser()
    {
        $db = new DB();
        $parameters = [request()->input("name"),request()->input("email"),password_hash(request()->input("password"),PASSWORD_BCRYPT),"user"];
        $register = $db->create("INSERT INTO users (name,email,password,type) VALUES (?,?,?,?)",$parameters);
        if ($register > 0){
            return true;
        }
        return false;
    }
}