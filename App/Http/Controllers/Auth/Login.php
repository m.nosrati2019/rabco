<?php namespace App\Http\Controllers\Auth;


use App\Database\DB;
use App\Request\Validation;
use Carbon\Carbon;

class Login
{
    public function __construct()
    {
        if (isLogin() == true){
            redirect("/");
        }
    }
    public function checkUser()
    {

               $validation = new Validation();
               $rules = [
                   "email" => "required|email",
                   "password" => "required",
               ];
               if ($validation->make($rules,request()->all()) == true){
                   if ($this->attempt() == false){
                       $errors = ['تعداد دفعات تلاش برای رمز عبور توسط شما به پایان رسیده است شما لطفا 20 دقیقه دیگر اقدام فرمایید.'];
                       return $errors;
                   }
                 $db = new DB();
                 $user = $db->rowCount("SELECT * FROM users where email=?",[request()->input("email")]);
                 if ($user == 1){
                     if ($this->checkPassword($data) == true){
                         $dateTime = Carbon::now("Asia/Tehran");
                         $ip = ip();
                         $db->create("UPDATE views SET attempt=1,updated_at=? WHERE ip=?",[$dateTime,$ip]);
                        if ($_SERVER['HTTP_REFERER'] != null){
                             back();
                        }else{
                             redirect("/");
                        }

                     }
                 }
                     $errors = ['کاربری با چنین مشخصاتی پیدا نشد!'];
                     return $errors;
               }
               $errors = $validation->getError();
               return $errors;
    }

    private function checkPassword($data)
    {

        $db = new DB();
        $user = $db->select("SELECT * FROM users where email=?",[request()->input("email")],1);
        $password = password_verify(request()->input("password"),$user->password);

        if ($password == true){
            $_SESSION['user'] = $user;
            return true;
        }
        return false;
    }

    private function attempt()
    {
        $db = new DB();
        $ip = ip();
        $attempt = $db->select("SELECT * FROM views WHERE ip=?",[$ip],1);
        if ($attempt == false){

            $db->create("INSERT INTO views (ip,attempt) VALUES (?,1)",[$ip]);
        }else{
            if ($attempt->attempt < 10){
                $dateTime = Carbon::now("Asia/Tehran");
                $db->create("UPDATE views SET attempt=attempt+1,updated_at=? WHERE ip=?",[$dateTime,$ip]);
            }
        }
        if (isset($attempt->attempt) and $attempt->attempt>= 10){
          if (Carbon::parse($attempt->updated_at,"Asia/Tehran")->diffInMinutes() > 20 ){
              $dateTime = Carbon::now("Asia/Tehran");
              $db->create("UPDATE views SET attempt=0,updated_at=? WHERE ip=?",[$dateTime,$ip]);
              return true;
          }
            return false;
        }
        return true;

    }
}