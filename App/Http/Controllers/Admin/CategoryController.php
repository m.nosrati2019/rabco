<?php namespace App\Http\Controllers\Admin;
use App\Database\DB;
use App\Request\Validation;


class CategoryController
{
    public function __construct()
    {

        if (checkAdmin() == false){
            redirect('/login.php');
        }
    }

    public function store()
    {
        $validate = new Validation();
        $rules = [
            "title" => "required",
            "description" => "required",
            "parent_id" => "required|number"
        ];
        if ($validate->make($rules,request()->all()) == true){
            $db = new DB();
            $parent_id = request()->input('parent_id');
            $category = $db->create("INSERT INTO categories (user_id,parent_id,title,meta_title,description) VALUES (?,?,?,?,?)",[auth()->id,isset($parent_id) ? $parent_id : 0,request()->input('title'),request()->input('title'),request()->input('description')]);
            if ($category == true){
                return true;
            }
        }
        $errors = $validate->getError();
        return $errors;

    }
}