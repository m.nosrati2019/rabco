<?php
namespace App\Database;
use config\Config;
use \PDO as PDO;
class DB
{
   protected $Pdo;
   private $dbName;
   private $password;
   private $username;
   private $host;
   private $option = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_CASE => PDO::CASE_NATURAL,
    PDO::ATTR_ORACLE_NULLS => PDO::NULL_EMPTY_STRING,
];

   public function __construct()
   {
   $config = new Config();
   $this->dbName = $config->boot()['dbName'];
   $this->username = $config->boot()['userName'];
   $this->password = $config->boot()['password'];
   $this->host = $config->boot()['host'];
  $this->Connection();

   }

    private function Connection()
    {
        if ($this->Pdo == null){
        try {
            $this->Pdo = new PDO("mysql:host={$this->host};dbname={$this->dbName};charset=utf8",$this->username,$this->password,$this->option);
        }catch (\PDOException $e){
            echo "Error Connection Failed! : ". $e->getMessage();
        }
        }
    }
 public function select(string $query,array $params = [],$fetch=0)
 {
    $stmt = $this->Pdo->prepare($query);
    if ($params != null){
        foreach ($params as $key=>$param){
            $stmt->bindValue($key+1,$param);
        }
    }
    $stmt->execute();
   if ($fetch==1){
       $result = $stmt->fetch(PDO::FETCH_OBJ);
   }else{
       $result = $stmt->fetchAll(PDO::FETCH_OBJ);
   }
    return $result;

  }
    public function rowCount(string $query,array $params = [])
    {
        $stmt = $this->Pdo->prepare($query);
        if ($params != null){
            foreach ($params as $key=>$param){
                $stmt->bindValue($key+1,$param);
            }
        }
        $stmt->execute();

        return $stmt->rowCount();

    }
    public function create(string $query,array $params=[]):bool
    {
        $stmt = $this->Pdo->prepare($query);
        if ($params != null){
            foreach ($params as $key=>$param){
                $stmt->bindValue($key+1,$param);
            }
        }
        $stmt->execute();
        if ($stmt->rowCount() > 0){
            return  true;
        }else{
            return false;
        }

    }


}