<?php
function isLogin(){
    if (isset($_SESSION['user'])){
        return true;
    }
    return false;
}
function dd(... $var){
    echo "<div style='background: #1c1c1c;color: #fff;'>";
    echo "<br>";
    var_dump($var);
    echo "<br>";
    echo "</div>";
    die();
}
function msg(string $message):void
{
    echo "<div style='background: #600202;color: #ffffff;text-align: center'>";
    echo "<br>";
    echo $message;
    echo "<br>";
    echo "</div>";
}
function view(string $path,array $item=[]) {
    if ($item != null){
        extract($item);
    }
    $pathEx = explode('.',$path);
    $pathFinal = implode($pathEx,DS);
    return include VIEW_PATH.$pathFinal.".php";
}
function assets(string $path):void{
    $pathEx = explode('/',$path);
    $pathFinal = implode($pathEx,DS);
    echo PUB_PATH.$pathFinal;
}
function action(string $path):void
{
    $pathEx = explode('.',$path);
    $pathFinal = implode($pathEx,DS);
    echo DS.$pathFinal.".php";
}
function redirect($path)
{
    $protocol = $_SERVER['SERVER_PROTOCOL'] == "HTTP" ? "http://" : "https://";
    header("Location: $path");
}
function back()
{
    header("Location: ".$_SERVER['HTTP_REFERER']);
}
function auth()
{
    if (isset($_SESSION['user'])){
        $db = new \App\Database\DB();
        $user = $db->select("SELECT * FROM users WHERE id=?",[$_SESSION['user']->id],1);
        return $user;
    }
}
function checkAdmin()
{
    if (isset($_SESSION['user'])){
        $db = new \App\Database\DB();
        $user = $db->rowCount("SELECT * FROM users WHERE type=?",['admin']);
        if ($user==1){return true;}
    }
    return false;
}
function ip()
{
    return $_SERVER['REMOTE_ADDR'];
}
function old($field) {
    return request($field);
}


function request($field = null) {
    $request = new App\Request\Request();
    if(is_null($field))
        return $request;

    return $request->input($field);
}