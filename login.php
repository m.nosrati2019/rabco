<?php
require_once "bootstrap/boot.php";
use App\Http\Controllers\Auth\Login;
$errors = '';
$login = new Login();
$loginInput = request()->input("login-button");
if (isset($loginInput) and $_SERVER['REQUEST_METHOD'] == 'POST'){
    $errors = is_array($login->checkUser()) ?  $login->checkUser() : "";
}
view("auth.login",compact("errors"));
