<?php
require_once "bootstrap/boot.php";
use App\Http\Controllers\Auth\Register;

$errors = '';
$register = new Register();
$registerInput = request()->input("register-button");
if (isset($registerInput) and $_SERVER['REQUEST_METHOD'] == 'POST'){
    $errors = is_array($register->register()) ?  $register->register() : "";
}
view("auth.register",compact("errors"));