<?php
session_start();
define("DS",DIRECTORY_SEPARATOR);
define("ROOT_PATH",dirname(__DIR__));
include ROOT_PATH.DS."bootstrap".DS."Constants.php";
include APP_PATH.DS."Helpers".DS."helpers.php";
include ROOT_PATH . DS . "config" . DS . "Config.php";
include ROOT_PATH.DS."vendor".DS."autoload.php";
use \App\Database\DB;